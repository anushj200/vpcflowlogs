
resource "aws_flow_log" "example" {
  log_destination      = "arn:aws:s3:::emmavpcflowlogsbucket123b"
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = "vpc-0dc2622e102d5f955" 
  log_format           = "$${version} $${account-id} $${vpc-id} $${subnet-id} $${instance-id} $${interface-id} $${srcaddr} $${dstaddr} $${srcport} $${dstport} $${pkt-srcaddr} $${pkt-dstaddr} $${protocol} $${type} $${packets} $${bytes} $${start} $${end} $${action} $${tcp-flags} $${log-status} $${region} $${az-id} $${sublocation-type} $${sublocation-id}"
}

