module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.70.0"
  name = "pexip"
  cidr = "10.0.0.0/16"
  azs  = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true
  one_nat_gateway_per_az = false
  enable_dns_hostnames = true
  enable_dns_support   = true
  enable_dhcp_options = true
  dhcp_options_domain_name = "test.com"
  dhcp_options_domain_name_servers = ["127.0.0.1", "10.0.0.2"]
  tags = {
    Terraform = "true"
    Environment = "pexip"
    Name = "pexip"
  }
}

resource "aws_flow_log" "example" {
  log_destination      = aws_s3_bucket.example.arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = module.vpc.vpc_id 
  log_format           = "$${version} $${account-id} $${vpc-id} $${subnet-id} $${instance-id} $${interface-id} $${srcaddr} $${dstaddr} $${srcport} $${dstport} $${pkt-srcaddr} $${pkt-dstaddr} $${protocol} $${type} $${packets} $${bytes} $${start} $${end} $${action} $${tcp-flags} $${log-status} $${region} $${az-id} $${sublocation-type} $${sublocation-id}"
}

resource "aws_s3_bucket" "example" {
  bucket = "emmavpcflowlogsbucket123"
}
